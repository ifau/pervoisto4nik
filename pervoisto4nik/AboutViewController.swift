//
//  AboutViewController.swift
//  pervoisto4nik
//
//  Created by ifau on 30/11/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController
{
    @IBOutlet var aboutTextView: UITextView!
    @IBOutlet var aboutTabusButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let descriptionText = "ООО \"Первоисточник\"\n\nАдрес: 141402, Московская обл, Химки г, Коммунальный проезд, владение 30\n\nОГРН: 1125047017479     ИНН: 5047136976"
        let attributedDescription = NSMutableAttributedString(string: descriptionText)
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue", size: 13)!, range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor(), range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSLinkAttributeName, value: NSURL(string: "http://maps.apple.com/?ll=55.912188,37.418533&z=16")!, range: (descriptionText as NSString).rangeOfString("141402, Московская обл, Химки г, Коммунальный проезд, владение 30"))
        
        aboutTextView.attributedText = attributedDescription
        aboutTextView.scrollEnabled = false
        
        aboutTabusButton.titleLabel?.textAlignment = NSTextAlignment.Center
        aboutTabusButton.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        aboutTabusButton.addTarget(self, action: "aboutTabusPressed", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func aboutTabusPressed()
    {
        self.performSegueWithIdentifier("aboutTabusSegue", sender: nil)
    }
    
    @IBAction func closeButtonPressed(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
