//
//  AboutTabusViewController.swift
//  pervoisto4nik
//
//  Created by ifau on 30/11/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class AboutTabusViewController: UIViewController
{
    @IBOutlet var aboutTextView: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let descriptionText = "Заходите\nСайт: app.tabus.ru\n\n\nЗвоните\nТелефон: +7 (495) 540-45-88\nSkype: ask_tabus\n\n\nПишите\nE-mail: app@tabus.ru\n\n\nПриезжайте\nАдрес: 141090, Россия, Московская обл., г. Королёв, ул. Московская, дом 3, офис 16\nВремя работы: пн-пт 9:00-18:00"
        
        let attributedDescription = NSMutableAttributedString(string: descriptionText)
        attributedDescription.addAttribute(NSFontAttributeName, value: UIFont(name: "HelveticaNeue", size: 13)!, range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0), range: NSRange(location: 0, length: descriptionText.characters.count))
        attributedDescription.addAttribute(NSLinkAttributeName, value: NSURL(string: "http://maps.apple.com/?ll=55.937982,37.862013&z=16")!, range: (descriptionText as NSString).rangeOfString("141090, Россия, Московская обл., г. Королёв, ул. Московская, дом 3, офис 16"))
        
        aboutTextView.attributedText = attributedDescription
        aboutTextView.scrollEnabled = false
    }

}
