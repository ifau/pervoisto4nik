//
//  CalcViewController.swift
//  pervoisto4nik
//
//  Created by ifau on 29/11/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class CalcViewController: UIViewController
{
    weak var delegate: rootCallbackDelegate?
    
    @IBOutlet private var heatSourceSwitch: AnimatedSegmentSwitch!
    @IBOutlet private var heatingTypeSwitch: AnimatedSegmentSwitch!
    @IBOutlet private var homeSpaceSwitch: AnimatedSegmentSwitch!
    
    private var sourceItems = ["Газовый котёл", "Тепловой насос", "Твердотопливный котёл", "Электрический котёл"]
    private var typeItems = ["Тёплый водяной пол", "Радиаторная система"]
    private var spaceItems = ["70-100", "100-150", "150-200", "200-250"]
    
    var descr : String
    {
        get
        {
            return "Источник тепла: \(sourceItems[heatSourceSwitch.selectedIndex])\nЖелаемый вид отопления: \(typeItems[heatingTypeSwitch.selectedIndex])\nПлощадь дома: \(spaceItems[homeSpaceSwitch.selectedIndex])"
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        heatSourceSwitch.items = sourceItems
        heatingTypeSwitch.items = typeItems
        homeSpaceSwitch.items = spaceItems

        for segmentSwitch in [heatSourceSwitch, heatingTypeSwitch, homeSpaceSwitch]
        {
            segmentSwitch.backgroundColor = UIColor.lightGrayColor()
            segmentSwitch.selectedTitleColor = greenColor
            segmentSwitch.cornerRadius = 0
            segmentSwitch.thumbCornerRadius = 0
            segmentSwitch.font = UIFont(name: "HelveticaNeue-Bold", size: 11.0)
        }
    }
    
    @IBAction func calculateButtonPressed(sender: AnyObject)
    {
        delegate?.buttonDidPressedInViewController(self)
    }
}
