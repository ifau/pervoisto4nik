//
//  RootViewController.swift
//  pervoisto4nik
//
//  Created by ifau on 29/11/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

let greenColor = UIColor(red: 64.0/255.0, green: 184.0/255.0, blue: 72.0/255.0, alpha: 1.0)

protocol rootCallbackDelegate:class
{
    func buttonDidPressedInViewController(viewController: UIViewController)
}

class RootViewController: UIViewController, UITextFieldDelegate, rootCallbackDelegate
{
    var pageMenu : CAPSPageMenu?
    
    var nameTextField : UITextField?
    var phoneTextField : UITextField?
    var emailTextField : UITextField?
    
    var requestType : Int!
    var requestDescription : String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initialize()
    }

    func initialize()
    {
        let logoImageView = UIImageView(image: UIImage(named: "Logo"))
        logoImageView.contentMode = .ScaleAspectFill
        self.navigationItem.titleView = logoImageView
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "IconInfo"), style: .Done, target: self, action: "infoButtonPressed")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "IconEmpty"), style: .Done, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = greenColor
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = storyboard.instantiateViewControllerWithIdentifier("MainVC") as! MainViewController
        let vc2 = storyboard.instantiateViewControllerWithIdentifier("AdvantageVC")
        let vc3 = storyboard.instantiateViewControllerWithIdentifier("HeatSourcesVC")
        let vc4 = storyboard.instantiateViewControllerWithIdentifier("HeatingTypesVC")
        let vc5 = storyboard.instantiateViewControllerWithIdentifier("CalculatorVC") as! CalcViewController
        vc1.delegate = self
        vc5.delegate = self
        
        let controllers = [vc1, vc2, vc3, vc4, vc5]

        let parameters: [CAPSPageMenuOption] =
        [
            .ScrollMenuBackgroundColor(UIColor.whiteColor()),
            .SelectionIndicatorColor(greenColor),
            .BottomMenuHairlineColor(greenColor),
            .SelectedMenuItemLabelColor(greenColor),
            .UnselectedMenuItemLabelColor(UIColor.blackColor()),
            .MenuItemFont(UIFont(name: "HelveticaNeue-Light", size: 13.0)!),
            .MenuHeight(40.0),
            .CenterMenuItems(true)
        ]

        pageMenu = CAPSPageMenu(viewControllers: controllers, frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMoveToParentViewController(self)
        
        let callButton = UIButton()
        callButton.backgroundColor = UIColor(red: 56.0/255.0, green: 175.0/255.0, blue: 56.0/255.0, alpha: 1.0) // wtf????
        callButton.setTitle("Заказать звонок", forState: .Normal)
        callButton.addTarget(self, action: "callButtonPressed", forControlEvents: .TouchUpInside)
        callButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(callButton)
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[button]-0-|", options: NSLayoutFormatOptions.DirectionLeftToRight, metrics: nil, views: ["button": callButton]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[button(==45)]-0-|", options: NSLayoutFormatOptions.DirectionLeftToRight, metrics: nil, views: ["button": callButton]))
    }
    
    func infoButtonPressed()
    {
        self.performSegueWithIdentifier("aboutSegue", sender: nil)
    }
    
    func policyButtonPressed()
    {
        self.performSegueWithIdentifier("policySegue", sender: nil)
    }
    
    func callButtonPressed()
    {
        buttonDidPressedInViewController(self)
    }
    
    func buttonDidPressedInViewController(viewController: UIViewController)
    {
        if viewController is MainViewController
        {
            requestType = 0
            requestDescription = "Подобрать варианты"
            configureAndPresentChoiceAlert()
        }
        else if viewController is RootViewController
        {
            requestType = 1
            requestDescription = "Позвоните мне"
            configureAndPresentPhoneAlert()
        }
        else if viewController is CalcViewController
        {
            requestType = 2
            requestDescription = "Расчёт суммы\n\((viewController as! CalcViewController).descr)"
            configureAndPresentCalcAlert()
        }
    }
    
    func configureAndPresentChoiceAlert()
    {
        let alertController = UIAlertController(title: "Заполните форму", message: "и мы подберем три ценовых варианта решений по отоплению для Вашего дома", preferredStyle: .Alert)

        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
            
            textField.placeholder = "Ваше имя*"
            textField.keyboardType = .Default
            textField.text = self.nameTextField?.text
            self.nameTextField = textField
        }
        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
                
            textField.placeholder = "Ваш телефон*"
            textField.keyboardType = .PhonePad
            textField.text = self.phoneTextField?.text
            textField.delegate = self
            self.phoneTextField = textField
        }
        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
                
            textField.placeholder = "Ваш e-mail"
            textField.keyboardType = .EmailAddress
            textField.text = self.emailTextField?.text
            self.emailTextField = textField
        }
        
        alertController.addAction(UIAlertAction(title: "Политика конфиденциальности", style: .Default, handler:
        { (alert: UIAlertAction!) in
            
            self.policyButtonPressed()
        }))
        
        alertController.addAction(UIAlertAction(title: "Подобрать", style: .Destructive, handler:
        { (alert: UIAlertAction!) in
                
            self.acceptButtonPressed()
        }))
        
        alertController.addAction(UIAlertAction(title: "Отмена", style: .Cancel, handler:
        { (alert: UIAlertAction!) in
            
        }))
        
        self.presentViewController(alertController, animated: true, completion:{})
    }
    
    func configureAndPresentPhoneAlert()
    {
        let alertController = UIAlertController(title: "Заказать звонок", message: nil, preferredStyle: .Alert)
        
        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
                
            textField.placeholder = "Ваше имя*"
            textField.keyboardType = .Default
            textField.text = self.nameTextField?.text
            self.nameTextField = textField
        }
        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
                
            textField.placeholder = "Ваш телефон*"
            textField.keyboardType = .PhonePad
            textField.text = self.phoneTextField?.text
            textField.delegate = self
            self.phoneTextField = textField
        }
        
        alertController.addAction(UIAlertAction(title: "Политика конфиденциальности", style: .Default, handler:
        { (alert: UIAlertAction!) in
            
            self.policyButtonPressed()
        }))
        
        alertController.addAction(UIAlertAction(title: "Позвоните мне!", style: .Destructive, handler:
        { (alert: UIAlertAction!) in
                
            self.acceptButtonPressed()
        }))
        
        alertController.addAction(UIAlertAction(title: "Отмена", style: .Cancel, handler:
        { (alert: UIAlertAction!) in
            
        }))
        
        self.presentViewController(alertController, animated: true, completion:{})
    }
    
    func configureAndPresentCalcAlert()
    {
        let alertController = UIAlertController(title: "Заполните форму", message: "чтобы узнать стоимость отопления Вашего дома", preferredStyle: .Alert)
        
        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
                
            textField.placeholder = "Ваше имя*"
            textField.keyboardType = .Default
            textField.text = self.nameTextField?.text
            self.nameTextField = textField
        }
        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
                
            textField.placeholder = "Ваш телефон*"
            textField.keyboardType = .PhonePad
            textField.text = self.phoneTextField?.text
            textField.delegate = self
            self.phoneTextField = textField
        }
        
        alertController.addTextFieldWithConfigurationHandler
        { (textField: UITextField) -> Void in
                
            textField.placeholder = "Ваш e-mail"
            textField.keyboardType = .EmailAddress
            textField.text = self.emailTextField?.text
            self.emailTextField = textField
        }
        
        alertController.addAction(UIAlertAction(title: "Политика конфиденциальности", style: .Default, handler:
        { (alert: UIAlertAction!) in
            
            self.policyButtonPressed()
        }))
        
        alertController.addAction(UIAlertAction(title: "Узнать стоимость", style: .Destructive, handler:
        { (alert: UIAlertAction!) in
                
            self.acceptButtonPressed()
        }))
        
        alertController.addAction(UIAlertAction(title: "Отмена", style: .Cancel, handler:
        { (alert: UIAlertAction!) in
                
        }))

        self.presentViewController(alertController, animated: true, completion:{})
    }
    
    func acceptButtonPressed()
    {
        if !((nameTextField != nil) && (nameTextField?.text?.characters.count > 0))
        {
            let alertController = UIAlertController(title: "", message: "Имя не может быть пустым", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "ОК", style: .Cancel, handler:nil))
            self.presentViewController(alertController, animated: true, completion:{})
        }
        else if !((phoneTextField != nil) && (phoneTextField?.text?.characters.count > 0))
        {
            let alertController = UIAlertController(title: "", message: "Номер телефона не может быть пустым", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "ОК", style: .Cancel, handler:nil))
            self.presentViewController(alertController, animated: true, completion:{})
        }
        else
        {
            let phone = formatPhone(phoneTextField!.text!)
            var personalInfoString = "\(nameTextField!.text!)"
            if emailTextField?.text?.characters.count > 0
            {
                personalInfoString += "\n\(emailTextField!.text!)"
            }
            requestDescription = "\(personalInfoString)\n\(requestDescription)"
            
            sendRequest(type: requestType, description: requestDescription, phone: phone)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 14
    }
    
    func sendRequest(type type: Int, description: String, phone: String)
    {
        let escapedDescription = description.stringByReplacingOccurrencesOfString("&", withString: "&amp;").stringByReplacingOccurrencesOfString("\"", withString: "&quot;").stringByReplacingOccurrencesOfString("'", withString: "&#39;").stringByReplacingOccurrencesOfString(">", withString: "&gt;").stringByReplacingOccurrencesOfString("<", withString: "&lt;")
        
        //.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let xmlString = "<xml><company>1</company><phone>\(phone)</phone><type>\(type)</type><description>\(escapedDescription)</description><platform>iOS</platform></xml>"
        
        let url = NSURL(string: "http://83.69.207.222:8325/sv_app/hs/app_processing/sendapp")!
        let request = NSMutableURLRequest(URL: url, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 20.0)
        
        let authString = NSString(string: "app:jG7YZUwK44")
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)!
        let authValue = "Basic \(authData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)))"
        
        request.setValue(authValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        request.HTTPBody = xmlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue())
        { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            
            var message : String!
            
            if error != nil
            {
                message = error!.localizedDescription
            }
            else if (response as! NSHTTPURLResponse).statusCode == 200
            {
                message = "Ваша заявка успешно отправлена"
            }
            else
            {
                message = "Не удалось отправить заявку"
            }
            
            dispatch_async(dispatch_get_main_queue(),
            { () -> Void in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "ОК", style: .Cancel, handler:nil))
                self.presentViewController(alertController, animated: true, completion:{})
            })
        }
    }
    
    func formatPhone(phone: String) -> String
    {
        var reversedString = ""
        var f = 0
        
        for (var i = phone.characters.count - 1; i >= 0; i--)
        {
            if (f == 2 || f == 4)
            {
                reversedString += "-"
            }
            else if (f == 7)
            {
                reversedString += " )"
            }
            reversedString += String([phone[phone.startIndex.advancedBy(i)]])
            f++
        }
        
        if f > 7
        {
            reversedString += "("
        }
        
        return String(reversedString.characters.reverse()) //(909) 555-66-77
    }
}
