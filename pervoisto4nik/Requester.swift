//
//  Requester.swift
//  pervoisto4nik
//
//  Created by ifau on 01/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class Requester: NSObject
{
    class func sendRequest(type type: Int, description: String, phone: String)
    {
        let escapedDescription = description.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let xmlString = "<xml><company>1</company><phone>\(phone)</phone><type>\(type)</type><description>\(escapedDescription)</description><platform>iOS</platform></xml>"
        
        let url = NSURL(string: "http://83.69.207.222:8325/sv_app/hs/app_processing/sendapp")!
        let request = NSMutableURLRequest(URL: url, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 20.0)
        
        let authString = NSString(string: "app:jG7YZUwK44")
        let authData = authString.dataUsingEncoding(NSUTF8StringEncoding)
        let authValue = "Basic \(authData?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)))"
        
        request.setValue(authValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        request.HTTPBody = xmlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue())
        { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                
        }
    }
}
