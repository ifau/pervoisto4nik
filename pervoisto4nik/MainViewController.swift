//
//  MainViewController.swift
//  pervoisto4nik
//
//  Created by ifau on 29/11/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class MainViewController: UIViewController
{
    weak var delegate: rootCallbackDelegate?

    @IBAction func choiceButtonDidPressed(sender: AnyObject)
    {
        delegate?.buttonDidPressedInViewController(self)
    }
}
